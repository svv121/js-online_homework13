"use strict";
/*
1. Функція setTimeout() зазвичай використовується, якщо треба запустити функцію один раз через певну кількість мілісекунд з моменту виклику setTimeout(). Функція setInterval() зазвичай використовується для встановлення затримки для функцій, які виконуються знову і знову регулярно після заданого інтервалу часу, якщо їй не наказано зупинитися за допомогою команди clearInterval(timerName).
2. Якщо встановити нульову затримку setTimeout(someFunction, 0), функция буде поставлена останньою в чергу по відношенню до поточного коду, тобто виклик функції буде запланований відразу після виконання поточного коду.
3. Функція clearInterval() очищає інтервал, який був встановлений функцією setInterval() до цього,
тоді виклік функції буде припинено, якщо цього не зробити, функція буде викликатися знову і знову.
*/

/*-------countdown----------*/
const counter = document.getElementsByClassName("counter")[0];
let timeCounter = 0;
let timeout = 3000;
let timer;
function countDown(){
        let start = Date.now() + timeout;
        let range;
        timer = setInterval(function () {
            range = start - Date.now();
            counter.innerHTML = "<span>" + range / 1000 + "</span>"
            if (range < 20) clearInterval(timer);
        }, 0);
    timeCounter = setTimeout(countDown, timeout)
}
/*-------image_changer----------*/
const imgCollection = document.querySelectorAll(".image-to-show");
let active=0;
let timerChange = 0;
let fadeTimer = 0;
function changeImage(){
    imgCollection.forEach((_img, index) => {
        if(index !== active){
            imgCollection[index].classList.remove('current');
        } else {
            imgCollection[index].classList.add('current');
        }
    })
    /*------------------------fadeIn---Start----------------*/
    let currentImage = imgCollection[active];
    function fadeIn(el, time) {
        el.style.opacity = "0";
        let last = +new Date();
        let tick = function() {
            el.style.opacity = (+el.style.opacity + (new Date() - last) / time).toString();
            last = +new Date();
            if (+el.style.opacity < 1) {
                (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
            }
        };
        tick();
    }
    fadeIn(currentImage, 500);
    /*------------------------fadeIn--END-----------------*/

    /*------------------------fadeOut---Start----------------*/
    function fadeOut() {
        let fadeEffect = setInterval(function () {
            currentImage.style.opacity = (currentImage.style.opacity - 0.1).toString();
            if (currentImage.style.opacity < 0) {
                clearInterval(fadeEffect);
            }
        }, 100);
    }
    fadeTimer = setTimeout(fadeOut, 2500);
    /*------------------------fadeOut--END-----------------*/

    if(active < imgCollection.length-1)
        {
            active += 1;}
    else
        {
            active = 0;}
    timerChange = setTimeout(changeImage, timeout);
}
/*---------------------------------*/
window.addEventListener('load', changeImage);
window.addEventListener('load', countDown);
const buttonPause = document.getElementById("pause-btn");
buttonPause.addEventListener("click", (event) => {
    if(event.target.classList.contains("btn-slider")){
        clearTimeout(timeCounter);
        clearTimeout(timerChange);
        clearTimeout(fadeTimer);
        clearInterval(timer);
    }
})
const buttonContinue = document.getElementById("continue-btn");
buttonContinue.addEventListener("click", (event) => {
    if(event.target.classList.contains("btn-slider")){
        clearTimeout(timeCounter);
        clearTimeout(timerChange);
        clearTimeout(fadeTimer);
        clearInterval(timer);
        active = active !== 0 ? active - 1 : imgCollection.length-1;
        changeImage();
        countDown();
    }
});